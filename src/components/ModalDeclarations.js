const modalWindowDeclarations = [
    {
      id: "1",
      title: "Do you want to delete this file?",
      text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
      color: "red",
      closeButton: true
    },
    {
      id: "2",
      title: "Do you really want to submit this???",
      text: "Once you do this, it would be over. Really???",
      color: "blue",
      closeButton: false
    }
  ]

  export default modalWindowDeclarations;